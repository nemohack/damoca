# Ideas

[dancer](https://www.dezeen.com/2013/04/25/made-by-humans-digital-installation-by-universal-everything/)

[Motion capture dance project](https://www.youtube.com/watch?v=b5MsptjpLQk)

[Kung Fu](https://www.thisiscolossal.com/2016/05/kung-fu-visualizations/)

[Asphyxia](https://www.thisiscolossal.com/2015/03/asphyxia-a-striking-fusion-of-dance-and-motion-capture-technology/)
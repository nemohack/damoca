%% just an example
% just plot actual datapoint 
function example_fct(lnh,data)
% fetch datapoint
idx = 1;
subjects = fieldnames(data);
for s = 1:numel(subjects)
    markers = fieldnames(data.(subjects{s}));
    for m = 1:numel(markers)
        d(idx,:) = data.(subjects{s}).(markers{m})(end,:); %#ok<AGROW>
        idx = idx + 1;
    end
end

% update line
set(lnh,'XData',d(:,1),'YData',d(:,2),'ZData',d(:,3),...
    'LineStyle','none',...
    'Marker','o',...
    'MarkerFaceColor','b',...
    'MarkerEdgeColor','none',...
    'MarkerSize',4);

drawnow;
end
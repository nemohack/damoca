function calibrationData = emgCalibrationFunc( myParams, viconClient, calibrationData )

% vicon settings
load( 'viconSettings.mat', 'viconSettings' );

%% settings
isRunning = true;
currentExperimentFrame = 0;
recordingTimeInFrames = 4*60; % length of data recording for emg
timeOfFixation = 2*60; % 2 s - length of visible fixation cross
samplesPerLoop = viconSettings.samplesPerLoop; % data recording is 960 hz at 60 hz runtime results in 16 sample per runtime loop


dataFields = {'noBlink', 'leftBlink', 'rightBlink'};

emgLeft = nan( recordingTimeInFrames, samplesPerLoop );
emgRight = nan( recordingTimeInFrames, samplesPerLoop );

fixationCross.width = 16;
fixationCross.height = 9;
fixationCross.scale = 20;
fixationCrossScaled = [0, 0, fixationCross.width*fixationCross.scale, ...
                       fixationCross.height*fixationCross.scale];
destinationRect = CenterRectOnPoint( fixationCrossScaled, ...
                                     myParams.screenWidth*0.5, myParams.screenHeight*0.5 );



stimulusBackground = 1/255*myParams.colors.foregroundColor;
crossImage = imread( 'Assets/crossB.png', 'png' ,'BackgroundColor', stimulusBackground );
crossTex = Screen( 'MakeTexture', myParams.window, crossImage );

while isRunning
    currentExperimentFrame = currentExperimentFrame + 1; % increment experiment frame counter
    
    if currentExperimentFrame >= recordingTimeInFrames
        isRunning = false;
    end
    
    if currentExperimentFrame < timeOfFixation % checks if the fixations cross has to be displayed
        Screen( 'DrawTexture', myParams.window, crossTex, [], destinationRect );
    end

    getViconData()
    
    Screen( 'Flip', myParams.window ); % draw frame
end
emgLeft = reshapeData( emgLeft, samplesPerLoop );
emgRight = reshapeData( emgRight, samplesPerLoop );

calibrationData.(dataFields{myParams.whichEye+1}).emgLeft = emgLeft;
calibrationData.(dataFields{myParams.whichEye+1}).emgRight = emgRight;

if myParams.whichEye == 2
    save( 'calibrationData.mat', 'calibrationData' )
    save( "data/" + num2str( myParams.subjectNr, '%02d' ) + "_" + myParams.nameString + "_calibration", ...
          'calibrationData' );
end

%% plots
emgLeftProcessed = processCalibrationFiles( emgLeft );
emgRightProcessed = processCalibrationFiles( emgRight );

figure(  'Position',  [100, 400, 1200, 600])
subplot( 1, 2, 1 )
plot( emgLeftProcessed.raw )
title( ['EMG Left - ', dataFields{myParams.whichEye+1}] )
subplot( 1, 2, 2 )
plot( emgRightProcessed.raw )
title( ['EMG Right - ', dataFields{myParams.whichEye+1}] )
drawnow();

function getViconData()
        viconDeviceCount = viconClient.GetDeviceCount().DeviceCount;
        
        viconClient.GetFrame();
            Output_GetFrameNumber = viconClient.GetFrameNumber();
%             frameLog(frameIdx) = Output_GetFrameNumber.FrameNumber;
           
%             disp(Output_GetFrameNumber)
%             Output_GetTimecode = MyClient.GetTimecode();
        if viconDeviceCount > 0
            for deviceIndex = 1 : viconDeviceCount
                % Gets the device name and type
                getDeviceName = viconClient.GetDeviceName( deviceIndex );
                % Counts the number of device outputs
                deviceOutputCount = viconClient.GetDeviceOutputCount( getDeviceName.DeviceName ).DeviceOutputCount;
                for deviceOutputIndex = 1 : deviceOutputCount
                    % Gets the device output name and unit
                    getDeviceOutputName = viconClient.GetDeviceOutputName( getDeviceName.DeviceName, deviceOutputIndex );

                    % Gets the number of subsamples associated with this device.
                    getDeviceOutputSubsamples = viconClient.GetDeviceOutputSubsamples( getDeviceName.DeviceName, ...
                        getDeviceOutputName.DeviceOutputName );

                    for deviceOutputSubsample = 1 : getDeviceOutputSubsamples.DeviceOutputSubsamples
                        getDeviceOutputValue = viconClient.GetDeviceOutputValue( getDeviceName.DeviceName, ...
                            getDeviceOutputName.DeviceOutputName, ...
                            deviceOutputSubsample ); % gets current value

                        % Decision where the value has to be stored
                        if deviceIndex == 2
                            switch deviceOutputIndex
                                case 1
                                    emgLeft(currentExperimentFrame,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 2
                                    emgRight(currentExperimentFrame,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                otherwise
                                    error( 'Error while receiving vicon data' );
                            end
                        end
                    end
                end
            end
        end
    end

% processes emg calibration data
    function processedFile = processCalibrationFiles( emgFile )
        % filter settings
        load( 'filterSettings.mat', 'filterSettings' )
        % generated by emgCalibration.m
        
        %% processing EMG calibration data
        processedFile = processEMG( emgFile, 1, filterSettings );
        
        % myParams.normalizedLeftBlink = myParams.noBlinkLeftProcessed.mean / blinkLeftProcessed;
        % myParams.normalizedRightBlink = myParams.noBlinkRightProcessed.mean / blinkRightProcessed;
    end

end

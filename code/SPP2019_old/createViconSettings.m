% creates a file with the vicon settings for the blinkStop experiment

screenRefreshrate = 60;

viconSettings.hostName = '127.0.0.1:801';
viconSettings.rate = 600;
viconSettings.samplesPerLoop = viconSettings.rate/screenRefreshrate;

save( 'viconSettings.mat', 'viconSettings' );
function emgProcessed = processEMG( emgData, parameter, settings )
%PROCESSEMG processes EMG data and returns key values of the processed
%file.
%   EMGPROCESSED = PROCESSEMG( EMGDATA, PARAMETER, SETTINGS ) filters,
%   smooths and rectifies raw EMG data. EMGPROCESSED returns depending on
%   the parameter PARAMETER either mean, std and max or only max. Parameter
%   SETTINGS is expected to be a struct including the fields rate, filterSpan,
%   filterOrder and filterCuttoff.
%   

%% processing settings
rate = settings.rate;
filterSpan = settings.filterSpan;
filterOrder = settings.filterOrder;
filterCutoff = settings.filterCutoff;

%% processing
[b,a] = butter( filterOrder, filterCutoff/(rate*0.5), 'high' ); % filter construction

% filters data
emgFiltered = filtfilt( b, a, emgData );

% smooths and rectyfies data
emgRectSmooth = smooth( abs( emgFiltered - mean( emgFiltered ) ), filterSpan );

%% parameter generation and returning values
if parameter == 1 % baseline
    % finds baseline for no-blink data
    emgProcessed.mean = mean( emgRectSmooth );
    emgProcessed.std  = std( emgRectSmooth );
    emgProcessed.max  = max( emgRectSmooth );
    emgProcessed.raw = emgRectSmooth;
elseif parameter == 2 % max
    % finds maximum for blink data
    emgProcessed = max( emgRectSmooth );
else
    error( 'Fehler, unbekannter Parameter' );
end

end


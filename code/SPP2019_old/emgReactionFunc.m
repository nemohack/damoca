function emgReactionFunc( myParams, viconClient)

%% settings
load( 'viconSettings.mat', 'viconSettings' );
load( 'filterSettings.mat', 'filterSettings' )
isRunning = true;
window = myParams.window;
flipInterval = myParams.flipInterval; % get the display refreshrate in ms

textSize.info = 30;
waitTime = 1;
waitCounter = round( waitTime / flipInterval );
trialTimeInFrames = round( 2.5 / flipInterval );
expTimeInFrames = 0;
stimulusDrawn = 0;

dataFields = {'copX', 'copY', 'copZ', 'comX', 'comY', 'comZ', ...
              'momentX', 'momentY', 'momentZ', 'emgLeft', 'emgRight'};
          
for currentField = 1 : length( dataFields )          
    expData.(dataFields{currentField}) = nan( trialTimeInFrames, viconSettings.samplesPerLoop );
end

while isRunning
    expTimeInFrames = expTimeInFrames + 1;
    if expTimeInFrames > waitCounter && ~stimulusDrawn
        %show stimulus
        Screen( 'TextSize', myParams.window, textSize.info );
        DrawFormattedText( window, 'JETZT!', 'center', ...
                           'center', myParams.colors.black );
        Screen( 'Flip', window);
        stimulusDrawn = 1;
    end
    getViconData()
    if expTimeInFrames > trialTimeInFrames
        isRunning = false;
        Screen( 'Flip', window);
    end
end

%% data processing
for currentField = 1 : length( dataFields )
    expData.(dataFields{currentField}) = reshapeData( expData.(dataFields{currentField}), ...
                                                      viconSettings.samplesPerLoop );
end

load( 'LeftBlink.mat', 'emgLeft' )
emgLeftCalibration = processEMG( emgLeft, 1, filterSettings );
load( 'RightBlink.mat', 'emgRight' )
emgRightCalibration = processEMG( emgRight, 1, filterSettings );
emgLeftProcessed = processEMG( expData.emgLeft, 1, filterSettings );
emgRightProcessed = processEMG( expData.emgRight, 1, filterSettings );

if emgLeftProcessed.max > emgRightProcessed.max && ...
        emgLeftProcessed.max > emgLeftCalibration.max * 0.5
    DrawFormattedText( window, 'Links', 'center', 'center', myParams.colors.black );
elseif emgLeftProcessed.max < emgRightProcessed.max && ...
        emgRightProcessed.max > emgRightCalibration.max * 0.5
    DrawFormattedText( window, 'Rechts', 'center', 'center', myParams.colors.black ); 
else
    DrawFormattedText( window, 'Nicht Reagiert', 'center', 'center', myParams.colors.black ); 
end
    DrawFormattedText( window, ['Max Links:' num2str( emgLeftProcessed.max )], ...
                       myParams.screenWidth * 0.2, 'center', myParams.colors.black );
    DrawFormattedText( window, ['Max Rechts:' num2str( emgRightProcessed.max )], ...
                       myParams.screenWidth * 0.6, 'center', myParams.colors.black );
    Screen( 'Flip', window);
    WaitSecs(1);
    Screen( 'Flip', window);
%% show result

function getViconData()
        viconDeviceCount = viconClient.GetDeviceCount().DeviceCount;
        
        viconClient.GetFrame();
%             Output_GetFrameNumber = viconClient.GetFrameNumber();
%             frameLog(frameIdx) = Output_GetFrameNumber.FrameNumber;
           
%             disp(Output_GetFrameNumber)
%             Output_GetTimecode = MyClient.GetTimecode();
        if viconDeviceCount > 0
            for deviceIndex = 1 : viconDeviceCount
                % Gets the device name and type
                getDeviceName = viconClient.GetDeviceName( deviceIndex );
                % Counts the number of device outputs
                deviceOutputCount = viconClient.GetDeviceOutputCount( getDeviceName.DeviceName ).DeviceOutputCount;
                if deviceIndex == 1
                    deviceOutputCount = 9; % lock to 9 parameters, raw data is ignored
                end
                for deviceOutputIndex = 1 : deviceOutputCount
                    % Gets the device output name and unit
                    getDeviceOutputName = viconClient.GetDeviceOutputName( getDeviceName.DeviceName, deviceOutputIndex );

                    % Gets the number of subsamples associated with this device.
                    getDeviceOutputSubsamples = viconClient.GetDeviceOutputSubsamples( getDeviceName.DeviceName, ...
                        getDeviceOutputName.DeviceOutputName );

                    for deviceOutputSubsample = 1 : getDeviceOutputSubsamples.DeviceOutputSubsamples
                        getDeviceOutputValue = viconClient.GetDeviceOutputValue( getDeviceName.DeviceName, ...
                            getDeviceOutputName.DeviceOutputName, ...
                            deviceOutputSubsample ); % gets current value

                        % Decision where the value has to be stored
                        if deviceIndex == 1
                            switch deviceOutputIndex
                                case 1
                                    expData.copX(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 2
                                    expData.copY(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 3
                                    expData.copZ(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 4
                                    expData.comX(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 5
                                    expData.comY(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 6
                                    expData.comZ(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 7
                                    expData.momentX(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 8
                                    expData.momentY(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 9
                                    expData.momentZ(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                otherwise
                                    error( 'Error while receiving vicon data' );
                            end
                        elseif deviceIndex == 2
                            switch deviceOutputIndex
                                case 1
                                    expData.emgLeft(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                case 2
                                    expData.emgRight(expTimeInFrames,deviceOutputSubsample) = getDeviceOutputValue.Value;
                                otherwise
                                    error( 'Error while receiving vicon data' );
                            end
                        end
                    end
                end
            end
        end
    end
end
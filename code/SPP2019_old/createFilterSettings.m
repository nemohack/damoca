%% calibration data processing
filterSettings.rate = viconSettings.rate; % should be equal to recording rate
filterSettings.filterSpan = 21;
filterSettings.filterOrder = 2;
filterSettings.filterCutoff = 5;

save( 'filterSettings.mat', 'filterSettings' );
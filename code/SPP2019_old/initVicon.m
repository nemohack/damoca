function viconClient = initVicon( hostName )
% Loads the SDK
Client.LoadViconDataStreamSDK();
viconClient = Client();

% Connects to a server
fprintf( 'Connecting to %s ...', hostName );
while ~viconClient.IsConnected().Connected
    viconClient.Connect( hostName );
    fprintf( '.' );
end
fprintf( '\n' );

% Enables some different data types
viconClient.EnableDeviceData();
fprintf( 'Device Data Enabled: %s\n', AdaptBool( viconClient.IsDeviceDataEnabled().Enabled ) );

% Sets the streaming mode
viconClient.SetStreamMode( StreamMode.ServerPush );
end
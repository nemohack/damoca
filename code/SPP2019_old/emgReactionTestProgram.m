% emg reaction test program

% vicon settings
load( 'viconSettings.mat', 'viconSettings' );

%% settings
whichScreen = 0;
textSize.info = 30;
myParams.whichKeyboard = -1;
isRunning = true;

% colors
myParams.colors.foregroundColor = [128, 128, 128]; 
myParams.colors.stimulusBackground = 1/255 * myParams.colors.foregroundColor;
myParams.colors.black = [0, 0, 0];sca
  

%% debug
Screen( 'Preference', 'SkipSyncTests', 0 );
myParams.isDebug = 1;
screenSizeWindowed = [];

%% Vicon initialization
viconClient = initVicon( viconSettings.hostName );

try
    % opens window
    ListenChar( 2 ); % prevents keyboard inputs in the background
    HideCursor(); % hides mouse cursor
    [myParams.window, screenSize] = Screen( 'OpenWindow', whichScreen,myParams.colors.foregroundColor, ...
                                            screenSizeWindowed );
    myParams.screenWidth = screenSize(3);
    myParams.screenHeight = screenSize(4);
    Screen( 'Blendfunction', myParams.window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    % get the display refreshrate in ms
    myParams.flipInterval = Screen( 'GetFlipInterval', myParams.window );
    
    while isRunning
        emgReactionFunc( myParams, viconClient )
        keepCheckingForInputs = true;
        while keepCheckingForInputs
            [keyIsDown, secs, keyCode] = KbCheck();
            if keyIsDown && keyCode(27)
                isRunning = false;
                keepCheckingForInputs = false;
            elseif keyIsDown && ~keyCode(27)
                keepCheckingForInputs = false;
            end
        end
    end
catch errorMessage
    %% cleanup if crashed
    % PTB
    Screen( 'CloseAll' )
    ListenChar( 0 )
    ShowCursor();
    % Audio
%     PsychPortAudio( 'Close', myParams.audioHandle );  % closes audio handle
    % Vicon
    viconCleanUp( viconClient );
    rethrow( errorMessage );
    
end


%% cleanup
% PTB
Screen( 'CloseAll' )
ListenChar( 0 )
ShowCursor();
% Audio
% PsychPortAudio( 'Close', myParams.audioHandle );  % closes audio handle
% Vicon
viconCleanUp( viconClient );


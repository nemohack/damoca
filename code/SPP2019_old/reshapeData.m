function dataOut = reshapeData( dataIn, samplesPerLoop )
dataOut = reshape( dataIn', [1, length( dataIn ) * samplesPerLoop] );
if isempty( dataOut(isnan( dataOut )) ) == 0
    dataOut(isnan( dataOut )) = [];
    disp( 'Deleted NaNs' )
end
end
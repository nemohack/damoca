%% calibration programm

%% input

disp( '0: Kein Blinzeln - 1: linkes Auge - 2: rechtes Auge' );
whichEye = input( 'Bitte geben Sie die Variante an: ' ); % Input name for filename

%% helper variables
isRunning = true;
currentExperimentFrame = 0;

recordingTimeInFrames = 4*60; % length of data recording for emg
timeOfFixation = 2*60; % 2 s - length of visible fixation cross
samplesPerLoop = 16; % data recording is 960 hz at 60 hz runtime results in 16 sample per runtime loop
whichKeyboard = -1;
whichScreen = 2;
screenSizeIfWindowed = [];
hostName = '127.0.0.1:801'; % ip adress of host pc
cBlack = [0, 0, 0];
emgLeft = nan( recordingTimeInFrames, samplesPerLoop );
emgRight = nan( recordingTimeInFrames, samplesPerLoop );

backgroundColor = BlackIndex( whichScreen );
foregroundColor = [128, 128, 128];
stimulusBackground = 1/255*foregroundColor;


%% reads image
picFormat = 'png';
crossImage = imread( 'Assets/crossB.png', picFormat ,'BackgroundColor', stimulusBackground );

%% initialization
% Loads the SDK
viconClient = initVicon( hostName );

%% inits screen
% opens window
ListenChar( 2 ); % prevents keyboard inputs in the background
HideCursor();
[window, screenSize]  = Screen( 'OpenWindow', whichScreen, foregroundColor, screenSizeIfWindowed );
Screen( 'Blendfunction', window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
crossTex = Screen( 'MakeTexture', window, crossImage );

%% instructions
DrawFormattedText( window, 'Bitte das Kreuz fixieren', ...
    'center', screenSize(4)*0.4, cBlack );
if whichEye == 0 
    timeOfFixation = recordingTimeInFrames;
elseif whichEye == 1 
    DrawFormattedText( window, 'Nach dem Verschwinden des Kreuzes mit dem linken Auge blinzeln', ...
        'center', 'center', cBlack );

elseif whichEye == 2
    DrawFormattedText( window, 'Nach dem Verschwinden des Kreuzes mit dem rechten Auge blinzeln', ...
        'center', 'center', cBlack );
end

DrawFormattedText( window, 'Weiter mit beliebiger Taste', ...
    'center', screenSize(4)*0.9, cBlack );

Screen( 'Flip', window );
% Wait for user input
KbWait( whichKeyboard );
KbReleaseWait( whichKeyboard );

%% Data collection
% checks if new sample (frame) is available
fprintf( 'Waiting for new frame...' );
while viconClient.GetFrame().Result.Value ~= Result.Success
    fprintf( '.' );
end
fprintf( '\n' );
% Output_GetFrameNumberIn = viconClient.GetFrameNumber();
while isRunning
    currentExperimentFrame = currentExperimentFrame + 1; % increment experiment frame counter
    
    if currentExperimentFrame >= recordingTimeInFrames
        isRunning = false;
    end
    
    if currentExperimentFrame < timeOfFixation % checks if the fixations cross has to be displayed
        Screen( 'DrawTexture', window, crossTex, [] );
    end

    [emgRight, emgLeft] = getViconData( viconClient, currentExperimentFrame, emgRight, emgLeft );
    
    Screen( 'Flip', window ); % draw frame
end
% Output_GetFrameNumberOut = viconClient.GetFrameNumber();
%% Cleanup
% PTB
Screen( 'CloseAll' )
ListenChar( 0 )
ShowCursor();

% Vicon
viconCleanUp( viconClient ) 

%% data processing
% change matrix to array
emgLeft = reshapeData( emgLeft, samplesPerLoop );
emgRight = reshapeData( emgRight, samplesPerLoop );

% save data
if whichEye == 0
    save( 'NoBlink.mat', 'emgLeft', 'emgRight' );
elseif whichEye == 1
    save( 'LeftBlink.mat', 'emgLeft', 'emgRight' );
elseif whichEye == 2
    save( 'RightBlink.mat', 'emgLeft', 'emgRight' );
else
    error( 'Fehler, angegebene Bedingung war falsch' )
end

%% functions
function [emgRight, emgLeft] = getViconData( viconClient, currentExperimentFrame, emgRight, emgLeft )



DeviceCount = viconClient.GetDeviceCount().DeviceCount;

viconClient.GetFrame();
% Output_GetFrameNumber = viconClient.GetFrameNumber();
% Output_GetTimecode = viconClient.GetTimecode();
if DeviceCount > 0
    for DeviceIndex = 1 : DeviceCount
        % Gets the device name and type
        getDeviceName = viconClient.GetDeviceName( DeviceIndex );
        % Counts the number of device outputs
        deviceOutputCount = viconClient.GetDeviceOutputCount( getDeviceName.DeviceName ).DeviceOutputCount;
        if DeviceIndex == 1
            deviceOutputCount = 9; %
        end
        for deviceOutputIndex = 1 : deviceOutputCount
            % Gets the device output name and unit
            getDeviceOutputName = viconClient.GetDeviceOutputName( getDeviceName.DeviceName, deviceOutputIndex );
            
            % Gets the number of subsamples associated with this device.
            getDeviceOutputSubsamples = viconClient.GetDeviceOutputSubsamples( getDeviceName.DeviceName, ...
                getDeviceOutputName.DeviceOutputName );
            
            for DeviceOutputSubsample = 1 : getDeviceOutputSubsamples.DeviceOutputSubsamples
                getDeviceOutputValue = viconClient.GetDeviceOutputValue( getDeviceName.DeviceName, ...
                    getDeviceOutputName.DeviceOutputName, ...
                    DeviceOutputSubsample ); % gets current value
                
                % Decision where the value has to be stored
                if DeviceIndex == 2
                    switch deviceOutputIndex
                        case 1
                            emgLeft(currentExperimentFrame,DeviceOutputSubsample) = getDeviceOutputValue.Value;
                        case 2
                            emgRight(currentExperimentFrame,DeviceOutputSubsample) = getDeviceOutputValue.Value;
                    end
                end
            end
        end
    end
end
end
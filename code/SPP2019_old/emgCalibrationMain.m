%% emgCalibrationMain

myParams.whichEye = 0;
% vicon settings
load( 'viconSettings.mat', 'viconSettings' );

%% experiment settings
whichScreen = 2;
screenSizeIfWindowed = [];
myParams.whichEye = 0;
% colors
myParams.colors.backgroundColor = BlackIndex( whichScreen );
myParams.colors.foregroundColor = [128, 128, 128];

myParams.colors.black = [0, 0, 0];
myParams.whichKeyboard = [];
forwardKey = KbName( 'f' );
repeatKey = KbName( 'w' );

%% Input
myParams = getSubjectData( myParams );
calibrationData.subject.nr = myParams.subjectNr;
calibrationData.subject.name = myParams.nameString;
calibrationData.subject.age = myParams.age;

% Loads the SDK
hostName = viconSettings.hostName; % ip adress of host pc
viconClient = initVicon( hostName );
try
    % opens window
    ListenChar( 2 ); % prevents keyboard inputs in the background
    HideCursor(); % hides mouse cursor
    [myParams.window, screenSize] = Screen( 'OpenWindow', whichScreen, myParams.colors.foregroundColor, ...
                                            screenSizeIfWindowed );
    myParams.screenWidth = screenSize(3);
    myParams.screenHeight = screenSize(4);
    Screen( 'Blendfunction', myParams.window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    % get the display refreshrate in ms
    myParams.flipInterval = Screen( 'GetFlipInterval', myParams.window );
    
    while myParams.whichEye < 3
        %% announcement
        drawInstruction( myParams, myParams.colors.black, myParams.whichEye )
        calibrationData = emgCalibrationFunc( myParams, viconClient, calibrationData );
        
        %% ask if successful
        drawTrialEndMessage( myParams, myParams.colors.black )
        disp( 'Kalibierung abgeschlossen' )
        if myParams.whichEye == 2
            disp( 'Zum Wiederholen (W) drücken, zum Beenden (F) drücken' );
        else
            disp( 'Zum Wiederholen (W) drücken, zum Fortfahren (F) drücken' );
        end
        waitingForInput = true;
        while waitingForInput
            [keyIsDown, ~, keyCode] = KbCheck();
            if keyIsDown
                if keyCode(forwardKey)
                    myParams.whichEye = myParams.whichEye + 1;
                    waitingForInput = false;
                elseif keyCode(repeatKey)
                    waitingForInput = false;
                end
            end
        end
        KbReleaseWait( myParams.whichKeyboard );
    end
    
catch errorMessage
    %% cleanup if crashed
    % PTB
    Screen( 'CloseAll' )
    ListenChar( 0 )
    ShowCursor();
    % Vicon
    viconCleanUp( viconClient );
    rethrow( errorMessage )
end

%% cleanup
% PTB
Screen( 'CloseAll' )
ListenChar( 0 )
ShowCursor();
% Vicon
viconCleanUp( viconClient );

function drawInstruction( myParams, inputColor, whichEye )
%% instructions
relativeMargin = 0.4;
DrawFormattedText( myParams.window, 'Bitte das Kreuz fixieren', ...
    'center', myParams.screenHeight*relativeMargin, inputColor );
if whichEye == 0
    DrawFormattedText( myParams.window, 'Bitte nicht blinzeln', ...
                      'center', 'center', inputColor );    
elseif whichEye == 1
    DrawFormattedText( myParams.window, 'Nach dem Verschwinden des Kreuzes mit dem linken Auge blinzeln', ...
                       'center', 'center', inputColor );
    
elseif whichEye == 2
    DrawFormattedText( myParams.window, 'Nach dem Verschwinden des Kreuzes mit dem rechten Auge blinzeln', ...
                       'center', 'center', inputColor );
end
relativeMargin = 0.9;
DrawFormattedText( myParams.window, 'Bitte Bereitschaft zum Weitermachen der Versuchsleiterin signalisieren', ...
                   'center', myParams.screenHeight*relativeMargin, inputColor );

Screen( 'Flip', myParams.window );
% Wait for user input
KbWait( myParams.whichKeyboard );
KbReleaseWait( myParams.whichKeyboard );
end

function drawTrialEndMessage( myParams, inputColor )
%% instructions


DrawFormattedText( myParams.window, 'Warten auf den Versuchsleiter', ...
                   'center', 'center', inputColor );    


Screen( 'Flip', myParams.window );
end

% gets subject input informations
function myParams = getSubjectData( myParams )
inputIsChecked = 0;
inputCorrectResponse = '';
while ~inputIsChecked
    myParams.nameString = input( ['Bitte geben Sie das Versuchspersonenkürzel ein ' ...
        '(Beispiel: Sheldon Cooper wird zu: SC): '], 's' ); % Input name for filename
    myParams.age = input( 'Bitte geben Sie das Alter der Versuchsperson an: ' ); % Input name for filename
    myParams.subjectNr = input( 'Bitte geben Sie die Versuchspersonen-Nummer ein: ' );
    
    % displays input parameters and asks for a check
    disp( '-------------------' )
    disp( '|      Check      |' )
    disp( '-------------------' )
    disp( ['VPN-Kürzel: ', myParams.nameString] )
    disp( ['Alter: ', num2str( myParams.age) ] )
    disp( ['Nr: ', num2str( myParams.subjectNr) ] )
    disp( '-------------------' )
    
    while ~strcmp( inputCorrectResponse, 'j' ) && ~strcmp( inputCorrectResponse, 'n' )
        inputCorrectResponse = input( 'Sind diese angaben korrekt? (j/n): ', 's' );
    end
    if strcmp( inputCorrectResponse, 'j' )
        inputIsChecked = 1;
    end
end
end
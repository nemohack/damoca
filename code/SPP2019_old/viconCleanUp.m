function viconCleanUp( viconClient )
% Disconnect and dispose
viconClient.Disconnect();

% Unload the SDK
fprintf( 'Unloading SDK...' );
Client.UnloadViconDataStreamSDK();
fprintf( 'done\n' );
end
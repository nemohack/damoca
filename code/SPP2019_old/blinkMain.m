% blink experiment

%% loads external files
% schedule
load( 'schedule.mat', 'schedule' );
% vicon settings
load( 'viconSettings.mat', 'viconSettings' );
% experiment memory
load( 'debugMemory.mat', 'drawDebugInfo' );
% loads subject info
load( 'calibrationData.mat', 'calibrationData' );

%% main struct that stores most important experiment parameters
myParams = struct;

%% processes EMG calibration files
myParams = processCalibrationFiles( myParams );

%% settings
whichScreen = 2;
textSize.info = 30;
myParams.whichKeyboard = -1;
nTrials = length( schedule.symbols );
% colors
myParams.colors.foregroundColor = [128, 128, 128];
myParams.colors.stimulusBackground = 1/255 * myParams.colors.foregroundColor;
myParams.colors.black = [0, 0, 0];
reactionTimeCounter = 0;
reactionTimeSave = [];
feedbackAfterNReactionTimes = 5;
showReactionTime = 0;

%% debug
myParams.isDebug = 0;
if myParams.isDebug == 0
    drawDebugInfo = 0;
end
Screen( 'Preference', 'SkipSyncTests', 0 );
screenSizeWindowed = [];

%% input
if ~myParams.isDebug
    myParams.nameString = calibrationData.subject.name;
    myParams.subjectNr = calibrationData.subject.nr;
else
    myParams.nameString = "TEST";
    myParams.subjectNr = 999;
    currentSubject = 999;
end

%% Audio
myParams.audioHandle = initAudio();

%% QUEST
questParams = createQuestParameter( nTrials );
questResult = QuestCreate( questParams.questThresholdGuess, questParams.questThresholdGuessSD, ...
                           questParams.questThresholdP, questParams.questBeta, ...
                           questParams.questDelta, questParams.questGamma );

%% Vicon initialization
viconClient = initVicon( viconSettings.hostName );

%% main exp
try
    % opens window
    ListenChar( 2 ); % prevents keyboard inputs in the background
    HideCursor(); % hides mouse cursor
    [myParams.window, screenSize] = Screen( 'OpenWindow', whichScreen, myParams.colors.foregroundColor, ...
                                    screenSizeWindowed );
    disp( 'Window is now OPEN' )
    myParams.screenWidth = screenSize(3);
    myParams.screenHeight = screenSize(4);
    Screen( 'Blendfunction', myParams.window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    % get the display refreshrate in ms
    myParams.flipInterval = Screen( 'GetFlipInterval', myParams.window ); 
    
    %% create stimuli
    stimuli = createStimuli( myParams.colors.stimulusBackground, schedule, myParams.window );
    
    %% main loop
    for currentTrial = 1 : nTrials
        
        myParams.currentSymbol = schedule.symbols(currentTrial); % symbol
        myParams.currentCondition = schedule.conditions(currentTrial); % kind of trial
        myParams.currentTrial = currentTrial;
          
        if currentTrial == 1
            if schedule.isBaseline(currentTrial) == 1
                textString = 'Bitte nur den Bildschirm betrachten und nicht reagieren';
                disp( 'Beobachtungsphase' );
                createInfoText( myParams, textSize, textString );
            end
        else
            if schedule.isBaseline(currentTrial) == 1 && schedule.isBaseline(currentTrial-1) == 0
                textString = 'Bitte nur den Bildschirm betrachten und nicht reagieren';
                disp( 'Beobachtungsphase' );
                createInfoText( myParams, textSize, textString );
            elseif schedule.isBaseline(currentTrial) == 0 && schedule.isBaseline(currentTrial-1) == 1
                textString = 'Bitte nun auf die kommenden Stimuli reagieren';
                disp( 'Reaktionsphase' );
                createInfoText( myParams, textSize, textString );
            end
        end
%         if schedule.isBaseline(currentTrial) == 0
%             trialStartMessage( myParams, textSize );
%         end
        
        
        %% Update Quest algorithm
        if myParams.currentCondition == schedule.is.Go_Right_Stop || ...
           myParams.currentCondition == schedule.is.Go_Left_Stop || ...
           currentTrial == 1 % only update if a stoptask was given
            myParams.questSuggestion = QuestQuantile( questResult ); % get new SOT from QUEST
            if myParams.questSuggestion < myParams.flipInterval
                myParams.questSuggestion = myParams.flipInterval;
            end
        end
        
        %% call exp function
        if myParams.isDebug
            load( 'debugMemory.mat', 'drawDebugInfo' ); 
            myParams.drawDebugInfo = drawDebugInfo;
        else
            myParams.drawDebugInfo = 0;
        end
        [reactionTime, reactionOnStop] = blinkFunc( myParams, schedule, stimuli, viconClient );
        
        if (myParams.currentCondition == schedule.is.Go_Right_Stop || ...
           myParams.currentCondition == schedule.is.Go_Left_Stop) && schedule.isBaseline(currentTrial) == 0
            questResult = QuestUpdate( questResult, myParams.questSuggestion, reactionOnStop );  
        end
        
        if (myParams.currentCondition == schedule.is.Go_Right || myParams.currentCondition == schedule.is.Go_Left) ...
                                                              && schedule.isBaseline(currentTrial) == 0
          reactionTimeSave = [reactionTime, reactionTimeSave];
          reactionTimeCounter = reactionTimeCounter+1;
          if reactionTimeCounter == feedbackAfterNReactionTimes
            meanRt = mean( reactionTimeSave );
            reactionTimeSave = [];
            showReactionTime = 1;
          end
        end
        
        % end trial info
        if showReactionTime
          trialEndMessageWithRT( myParams, textSize, currentTrial, reactionTime, schedule )
          showReactionTime = 0;
        elseif ~showReactionTime
          trialEndMessage( myParams, textSize, currentTrial, reactionTime, schedule, reactionOnStop )
        end
    end
catch errorMessage
    %% cleanup if crashed
    % PTB
    Screen( 'CloseAll' )
    ListenChar( 0 )
    ShowCursor();
    % Audio
    PsychPortAudio( 'Close', myParams.audioHandle );  % closes audio handle
    % Vicon
    viconCleanUp( viconClient );
    rethrow( errorMessage );

end

%% cleanup
% PTB
Screen( 'CloseAll' )
ListenChar( 0 )
ShowCursor();
% Audio
PsychPortAudio( 'Close', myParams.audioHandle );  % closes audio handle
% Vicon
viconCleanUp( viconClient );

%% functions
% creates text for info about baseline or regular trials
function createInfoText(myParams, textSize, textString)
relativeMargin = 0.9;
Screen( 'TextSize', myParams.window, textSize.info );
DrawFormattedText( myParams.window, textString, 'center', 'center', myParams.colors.black );
DrawFormattedText( myParams.window, 'Weiter mit beliebiger Taste', ...
    'center', myParams.screenHeight*relativeMargin, myParams.colors.black );
Screen( 'Flip', myParams.window);
KbWait( myParams.whichKeyboard );
KbReleaseWait( myParams.whichKeyboard );
end

% creates the message at the end of every trial
function trialEndMessage(myParams, textSize, currentTrial, reactionTime, schedule, reactionOnStop)
relativeMargin = 0.9;
Screen( 'TextSize', myParams.window, textSize.info );
if false
    DrawFormattedText( myParams.window, ['Reacted on stop: ' num2str( reactionOnStop )], 'center', ...
                      myParams.screenHeight*0.1, myParams.colors.black );
    DrawFormattedText( myParams.window, ['QUEST: ' num2str( myParams.questSuggestion )], 'center', ...
                      myParams.screenHeight*0.2, myParams.colors.black );                  
end
DrawFormattedText( myParams.window, ['links: ' schedule.labelsSingle{2} '    -    rechts: ' schedule.labelsSingle{3}], 'center', ...
                  myParams.screenHeight*0.3, myParams.colors.black );
DrawFormattedText( myParams.window, ['Ende Trial ' num2str( currentTrial )], 'center', ...
                   'center', myParams.colors.black );
DrawFormattedText( myParams.window, 'Weiter mit beliebiger Taste', ...
    'center', myParams.screenHeight*relativeMargin, myParams.colors.black );
if myParams.isDebug
    relativeMargin = 0.7;
    DrawFormattedText( myParams.window, ['RT: ' num2str( reactionTime )], 'center', ...
                       myParams.screenHeight*relativeMargin, myParams.colors.black ); 
end
Screen( 'Flip', myParams.window);
disp( ['Trial ', num2str( currentTrial ), ' is over - please press a key'] )
KbWait( myParams.whichKeyboard );
KbReleaseWait( myParams.whichKeyboard );
end

% creates the message at the end of a trial when RT's are returned
function trialEndMessageWithRT(myParams, textSize, currentTrial, reactionTime, schedule)
relativeMargin = 0.9;
relativeMarginRT = 0.7;
Screen( 'TextSize', myParams.window, textSize.info );

DrawFormattedText( myParams.window, ['links: ' schedule.labelsSingle{2} ' - rechts: ' schedule.labelsSingle{3}], 'center', ...
                  myParams.screenHeight*0.3, myParams.colors.black );
DrawFormattedText( myParams.window, ['Ende Trial ' num2str( currentTrial )], 'center', ...
                   'center', myParams.colors.black );
DrawFormattedText( myParams.window, ['Mittlere Reaktionszeit der letzten f�nf Reaktionen ' num2str( reactionTime )], 'center', ...
                   myParams.screenHeight*relativeMarginRT, myParams.colors.black );
DrawFormattedText( myParams.window, 'Weiter mit beliebiger Taste', ...
    'center', myParams.screenHeight*relativeMargin, myParams.colors.black );
Screen( 'Flip', myParams.window);
disp( ['Trial ', num2str( currentTrial ), ' is over - please press a key'] )
KbWait( myParams.whichKeyboard );
KbReleaseWait( myParams.whichKeyboard );
end

% creates the message at the start of every trial
function trialStartMessage(myParams, textSize)
relativeMargin = 0.9;
Screen( 'TextSize', myParams.window, textSize.info );
DrawFormattedText( myParams.window, 'Bitte reagieren Sie mit einem Blinzeln', 'center', ...
                   'center', myParams.colors.black );
DrawFormattedText( myParams.window, 'Weiter mit beliebiger Taste', ...
    'center', myParams.screenHeight*relativeMargin, myParams.colors.black );
Screen( 'Flip', myParams.window);
disp( 'Trial ready for start - please press a key' )
KbWait( myParams.whichKeyboard );
KbReleaseWait( myParams.whichKeyboard );
end

% initializes the quest algorithm
function questParams = createQuestParameter( lengthSchedule )
questParams.questAll = nan( 1, lengthSchedule );
questParams.questBeta = 3.5;
questParams.questDelta = 0.01;
questParams.questGamma = 0;

questParams.questThresholdGuess = 0.4; % not assumed reaction time, time to avoid reacting
questParams.questThresholdGuessSD = 0.2;
questParams.questThresholdP = 0.5;
end

% creates textures of the stimuli images
function stimulus = createStimuli( backgroundColor, schedule, window )
for currentImage = 1 : length( schedule.filenames )
    stimulus.(schedule.labelsSingle{currentImage}) = currentImage;
    tempImage = imread( schedule.filenames{currentImage}, 'png', 'BackgroundColor', backgroundColor );
    stimulus.textures{currentImage} = Screen( 'MakeTexture', window, tempImage );
end
end

% processes emg calibration data
function myParams = processCalibrationFiles( myParams )
% filter settings
load( 'filterSettings.mat', 'filterSettings' )
% generated by emgCalibration.m
load( 'calibrationData.mat', 'calibrationData' )
noBlink = calibrationData.noBlink; % stores no-blink emg data
blinkLeft = calibrationData.leftBlink; % stores blink left emg data
blinkRight = calibrationData.rightBlink; % stores blink right emg data

%% processing EMG calibration data
myParams.noBlinkLeftProcessed = processEMG( noBlink.emgLeft, 1, filterSettings );
myParams.noBlinkRightProcessed = processEMG( noBlink.emgRight, 1, filterSettings );
myParams.blinkLeftProcessed = processEMG( blinkLeft.emgLeft, 1, filterSettings );
myParams.blinkRightProcessed = processEMG( blinkRight.emgRight, 1, filterSettings );

% myParams.normalizedLeftBlink = myParams.noBlinkLeftProcessed.mean / blinkLeftProcessed;
% myParams.normalizedRightBlink = myParams.noBlinkRightProcessed.mean / blinkRightProcessed;
end

% initialized audio
function audiohandle = initAudio()
audioVolume = 0.1;
deviceId = [];
[audioData, audioFrequency] = psychwavread( 'Assets/beep-02.wav' ); % load soundfile
waveData = audioData'; % convert audiodata
% checks if the file was mono or stereo. If it's mono the channel will be doubled to fake stereo
nChannels = size( waveData, 1 );
if nChannels < 2 % if mono
  waveData = [waveData; waveData]; % make stereo
  nChannels = 2;
end

%% Audio initialization
InitializePsychSound();
audiohandle = PsychPortAudio( 'Open', deviceId, [], 2, audioFrequency, 2 );

PsychPortAudio( 'FillBuffer', audiohandle, waveData );
PsychPortAudio( 'Volume', audiohandle, audioVolume );
end
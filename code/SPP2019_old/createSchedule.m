% creates Schedule for SPP Experiment

rng(1);

% descriptions of the trials
reactionDescriptionStrings = {'Go_Right', 'Go_Left', ...
                              'Go_Right_Stop', 'Go_Left_Stop', ...
                              'Nogo_Symbol', 'Nogo_NoSymbol'};

% filenames of the graphics                          
filenames = {'Assets/NB.png', 'Assets/oB.png', 'Assets/xB.png', 'Assets/crossB.png' };

% labels of the graphics
labelsSingle = {'N', 'O', 'X', 'FixationCross'};

% only the smybols 1:3 are permuted (counter balancing)
nSymbols = 3;
symbolsGroups = perms( 1:nSymbols );

% for testing purpose one group is selected
thisGroup = symbolsGroups(1,:); 
%% settings                                      
nTrials = 160;
nBaselineTrials = 48;

%% schedule creation
% create trials array
[goTrials, nogoTrials] = createTrials( nTrials );
[goTrialsBaseline, nogoTrialsBaseline] = createTrials( nBaselineTrials*0.5 );

% create symbol array
[goSymbols, nogoSymbols] = createSymbols( nTrials, thisGroup );
[goSymbolsBaseline, nogoSymbolsBaseline] = createSymbols( nBaselineTrials*0.5, thisGroup );

%% schedule creation
% shuffle trials and save index for symbols
[schedule.conditions, idx] = Shuffle( [goTrials.right, goTrials.left, goTrials.rightStop, ...
                                    goTrials.leftStop, nogoTrials.symbol, nogoTrials.noSymbol] );
tempSchedule = [goSymbols.right, goSymbols.left, goSymbols.rightStop, ...
                                    goSymbols.leftStop, nogoSymbols.symbol, nogoSymbols.noSymbol];
% shuffle symbols accordingly                                 
schedule.symbols = tempSchedule(idx);

% baseline off trials
schedule.isBaseline = zeros( 1, length( schedule.symbols ) );

% create baseline trials and save index for symbols
[baselineSchedule.trials01, idx] = Shuffle( [goTrialsBaseline.right, goTrialsBaseline.left, ...
                                             goTrialsBaseline.rightStop, goTrialsBaseline.leftStop, ...
                                             nogoTrialsBaseline.symbol, nogoTrialsBaseline.noSymbol] );
                                         
baselineTemp = [goSymbolsBaseline.right, goSymbolsBaseline.left, ...
                goSymbolsBaseline.rightStop, goSymbolsBaseline.leftStop, ...
                nogoSymbolsBaseline.symbol, nogoSymbolsBaseline.noSymbol];   
            
% shuffle symbols accordingly     
baselineSchedule.symbols01 = baselineTemp(idx);            
                                   
[baselineSchedule.trials02, idx] = Shuffle( [goTrialsBaseline.right, goTrialsBaseline.left, ...
                                             goTrialsBaseline.rightStop, goTrialsBaseline.leftStop, ...
                                             nogoTrialsBaseline.symbol, nogoTrialsBaseline.noSymbol] ); 

% shuffle symbols accordingly   
baselineSchedule.symbols02 = baselineTemp(idx);                                   

baselineSchedule.isBaseline = ones( 1, length( baselineSchedule.trials01 ) );

schedule.conditions = [baselineSchedule.trials01, schedule.conditions(1:80), ...
                    baselineSchedule.trials02, schedule.conditions(81:160)];

schedule.isBaseline = [baselineSchedule.isBaseline, schedule.isBaseline(1:80), ...
                       baselineSchedule.isBaseline, schedule.isBaseline(81:160)];
                   
schedule.symbols = [baselineSchedule.symbols01, schedule.symbols(1:80), ...
                    baselineSchedule.symbols02, schedule.symbols(81:160)];                 

for currentSymbol = 1 : length( schedule.symbols )
    testValue = schedule.conditions(currentSymbol);
    descriptionSchedule{currentSymbol} = reactionDescriptionStrings{testValue};
end

for currentSymbol = 1 : length( schedule.symbols )
    testValue = schedule.symbols(currentSymbol);
    if testValue <= 3
        labelsSchedule{currentSymbol} = labelsSingle{testValue};
    else
        labelsSchedule{currentSymbol} = nan;
    end
end

for currentSymbol = 1 : length( reactionDescriptionStrings )
    schedule.is.(reactionDescriptionStrings{currentSymbol}) = currentSymbol;
end

schedule.labels = labelsSchedule;
schedule.reactionType = descriptionSchedule;
schedule.labelsSingle = labelsSingle;
schedule.filenames = filenames;

save( 'schedule.mat', 'schedule' );

%% reaction struct

reaction.is.leftBlink = 1;
reaction.is.rightBlink = 2;
reaction.is.noBlink = 3;

reaction.labels = {'Left Blink', 'Right Blink', 'No Blink'};

save( 'reaction.mat', 'reaction' );

%% experimental Brain
drawDebugInfo = 1;
save( 'debugMemory.mat', 'drawDebugInfo' );
                   
function [goTrials, nogoTrials] = createTrials( nTrials)
    % relative trial occurance                          
    relativeGoTrials.right = 0.25;
    relativeGoTrials.left = 0.25;

    relativeGoTrials.rightStop = 0.125;
    relativeGoTrials.leftStop = 0.125;

    relativeNogoTrials.symbol = 0.125;
    relativeNogoTrials.noSymbol = 0.125;

    %% array creation
    % go trials
    goTrials.right = ones( 1, nTrials * relativeGoTrials.right ) * 1;
    goTrials.left = ones( 1, nTrials * relativeGoTrials.left ) * 2;
    % go trials with stop
    goTrials.rightStop = ones( 1, nTrials * relativeGoTrials.rightStop ) * 3;
    goTrials.leftStop = ones( 1, nTrials * relativeGoTrials.leftStop ) * 4;
    % nogo trials
    nogoTrials.symbol = ones( 1, nTrials * relativeNogoTrials.symbol ) * 5;
    nogoTrials.noSymbol = ones( 1, nTrials * relativeNogoTrials.noSymbol ) * 6;
end

function [goTrials, nogoTrials] = createSymbols( nTrials, symbols)
    % relative trial occurance                          
    relativeGoTrials.right = 0.25;
    relativeGoTrials.left = 0.25;

    relativeGoTrials.rightStop = 0.125;
    relativeGoTrials.leftStop = 0.125;

    relativeNogoTrials.symbol = 0.125;
    relativeNogoTrials.noSymbol = 0.125;

    %% array creation
    % go trials
    goTrials.right = ones( 1, nTrials * relativeGoTrials.right ) * symbols(1);
    goTrials.left = ones( 1, nTrials * relativeGoTrials.left ) * symbols(2);
    % go trials with stop
    goTrials.rightStop = ones( 1, nTrials * relativeGoTrials.rightStop ) * symbols(1);
    goTrials.leftStop = ones( 1, nTrials * relativeGoTrials.leftStop ) * symbols(2);
    % nogo trials
    nogoTrials.symbol = ones( 1, nTrials * relativeNogoTrials.symbol ) * symbols(3);
    nogoTrials.noSymbol = ones( 1, nTrials * relativeNogoTrials.noSymbol ) * nan;
end
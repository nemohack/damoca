%% VICON SDK DataStreaming

TransmitMulticast = false;
Client.LoadViconDataStreamSDK();
HostName = '192.168.1.2:801'; 
MyClient = Client();

while ~MyClient.IsConnected().Connected
    % Direct connection
    MyClient.Connect( HostName );
end

MyClient.EnableMarkerData();
Output = MyClient.SetStreamMode( StreamMode.ClientPull);  %
MyClient.SetAxisMapping( Direction.Forward, Direction.Left, Direction.Up );    % Z-up
Latency = MyClient.GetLatencyTotal;

% axis(handles.axes1);
S = Screen('Screens');
R = Screen('Resolution',S(1));
W = R.width;
H = R.height;


figure('Position',[0 0 W H]),
hold on,
axis off,
box off,
h = plot3(0, 0, 0, 'ok', 'MarkerFaceColor', 'w');

while MyClient.GetFrame().Result.Value ~= Result.Success
end
SubjectName = MyClient.GetSubjectName( 1 ).SubjectName;
% Count the number of markers
MarkerCount = MyClient.GetMarkerCount( SubjectName ).MarkerCount;

MarkerTest = NaN(3,6);
BS = KbName('Backspace');
keyCode = zeros(1,256);

while  keyCode(BS) ~= 1
    [keyIsDown, secs, keyCode, deltaSecs] = KbCheck;
    
    annotation('Textbox',[.4 .7 .1 .1],'String','Alle Marker an der richtigen Stelle? Weiter mit "Backspace"!',...
        'FontSize', 20,'Color',[1 0 0]),
    
    %Warten bis neues Frame abgeholt wurde
    while MyClient.GetFrame().Result.Value ~= Result.Success
    end
    
    
    for MarkerIndex = 1 : MarkerCount
        % Get the marker name
        MarkerName = MyClient.GetMarkerName( SubjectName, MarkerIndex ).MarkerName;
        
        % Get the marker parent
        MarkerParentName = MyClient.GetMarkerParentName( SubjectName, MarkerName ).SegmentName;
        
        % Get the global marker translation
        Output_GetMarkerGlobalTranslation = MyClient.GetMarkerGlobalTranslation( SubjectName, MarkerName );
        MarkerTest(:,MarkerIndex) = Output_GetMarkerGlobalTranslation.Translation(1:3);
    end
    
    %for col = 1 : size(MarkerTest, 2)
        %plot3(MarkerTest(1, col),  MarkerTest(2,col), MarkerTest(3,col), 'ok', 'MarkerFaceColor', 'b');
    plot3(MarkerTest(1,1), MarkerTest(2,1), MarkerTest(3,1), 'ok', 'MarkerFaceColor', 'b');
    hold on
    plot3(MarkerTest(1,2), MarkerTest(2,2), MarkerTest(3,2), 'ok', 'MarkerFaceColor', 'g');
    plot3(MarkerTest(1,3), MarkerTest(2,3), MarkerTest(3,3),'ok', 'MarkerFaceColor', 'g');
    plot3(MarkerTest(1,4), MarkerTest(2,4), MarkerTest(3,4), 'ok', 'MarkerFaceColor', 'r');
    plot3(MarkerTest(1,5), MarkerTest(2,5), MarkerTest(3,5),'ok', 'MarkerFaceColor', 'r');
    plot3(MarkerTest(1,6), MarkerTest(2,6), MarkerTest(3,6),'ok', 'MarkerFaceColor', 'r');
    axis([-10 2000 -10 2000 -10 2000]),
    hold off
    pause(.001),
end


% Disconnect and dispose
MyClient.Disconnect();

% Unload the SDK
fprintf( 'Unloading SDK...' );
Client.UnloadViconDataStreamSDK();
fprintf( 'done\n' );

% close all
close all

%% Load data
load('sample_data.mat');
fields = fieldnames(data);

%% Settings
lag = 99;
incoming_data = 1;
start = 260;
T = 800;

%% Init plot settings
addpath('BrewerMap')
n_markers = lag + 1; % needs to be odd number 
rand_markers = (-10 + (10 + 10)*(rand(n_markers, 3)));
brewermap('RdYlGn'); % Preselect any colorscheme
map = brewermap(n_markers); % Use preselected scheme, select colormap length.

%% Plot
figure()
plot3(1000, 1000, 1000)
hold on
plot3(-1000, -1000, -1000)
lim = 2500;
xlim([1150, 1800]); ylim([-2500, 2500]); zlim([0,800]);
while incoming_data
    for subjectIdx = 1 : numel(fields)
        subject = data.(fields{subjectIdx});
        m_fields = fieldnames(subject);
        for marker = 1 : numel(m_fields)
            current_data = subject.(m_fields{marker})(start - lag : start, :);
            plot_data = current_data + rand_markers;
            fig = plot3(plot_data(:,1), plot_data(:, 2), plot_data(:,3), ...
                'Marker', 'o', ...
                'MarkerSize', 8, ...
                'MarkerFaceColor', map(1, :), ...
                'MarkerEdgeColor', map(1,:));
        end 
    end
    pause(0.005)
    cla
    start = start + 1;
    if start == T
        incoming_data = 0;
    end
end
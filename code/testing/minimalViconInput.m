
viconSettings.hostName = '192.168.1.2:801';


% Start Session
% Loads the SDK
Client.LoadViconDataStreamSDK();
viconClient = Client();

% Connects to a server
fprintf( 'Connecting to %s ...', viconSettings.hostName );
while ~viconClient.IsConnected().Connected
    viconClient.Connect( viconSettings.hostName );
    fprintf( '.' );
end
fprintf( '\n' );

% Enables some different data types
viconClient.EnableDeviceData();
fprintf( 'Device Data Enabled: %s\n', AdaptBool( viconClient.IsDeviceDataEnabled().Enabled ) );

% Sets the streaming mode
viconClient.SetStreamMode( StreamMode.ServerPush );

% get devie count
viconDeviceCount = viconClient.GetDeviceCount().DeviceCount;

viconClient.GetFrame();
Output_GetFrameNumber = viconClient.GetFrameNumber();
% frameLog(frameIdx) = Output_GetFrameNumber.FrameNumber;

%             disp(Output_GetFrameNumber)
%             Output_GetTimecode = MyClient.GetTimecode();
disp(viconDeviceCount)
if viconDeviceCount > 0
    disp("Devices Found")
    for deviceIndex = 1 : viconDeviceCount
        % Gets the device name and type
        getDeviceName = viconClient.GetDeviceName( deviceIndex );
        % Counts the number of device outputs
        deviceOutputCount = viconClient.GetDeviceOutputCount( getDeviceName.DeviceName ).DeviceOutputCount;
        if deviceIndex == 1
            deviceOutputCount = 9; % lock to 9 parameters, raw data is ignored
        end
        for deviceOutputIndex = 1 : deviceOutputCount
            % Gets the device output name and unit
            getDeviceOutputName = viconClient.GetDeviceOutputName( getDeviceName.DeviceName, deviceOutputIndex );
            
            % Gets the number of subsamples associated with this device.
            getDeviceOutputSubsamples = viconClient.GetDeviceOutputSubsamples( getDeviceName.DeviceName, ...
                getDeviceOutputName.DeviceOutputName );
            
            for deviceOutputSubsample = 1 : getDeviceOutputSubsamples.DeviceOutputSubsamples
                getDeviceOutputValue = viconClient.GetDeviceOutputValue( getDeviceName.DeviceName, ...
                    getDeviceOutputName.DeviceOutputName, ...
                    deviceOutputSubsample ); % gets current value
                
                % Decision where the value has to be stored
                
                data = getDeviceOutputValue.Value;
                
            end
        end
    end
end

% Disconnect and dispose
viconClient.Disconnect();

% Unload the SDK
fprintf( 'Unloading SDK...' );
Client.UnloadViconDataStreamSDK();
fprintf( 'done\n' );
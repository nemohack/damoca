workdir = pwd
addpath('BrewerMap')

n_markers = 11 % needs to be odd number 
marker = [5, 5, 5]
rand_markers = (-10 + (10 + 10)*(rand(n_markers, 3)))
all_markers = rand_markers + marker

brewermap('RdYlGn'); % Preselect any colorscheme
map = brewermap(n_markers); % Use preselected scheme, select colormap length.

figure()
plot3(all_markers(:, 1), all_markers(:,2), all_markers(:,3), ...
  'MarkerSize', 8, ...
 % 'MarkerEdgeColor', map, ...
  'MarkerFaceColor', map)
 


function viconClient = vicon_connect(opt)
% Loads the SDK
Client.LoadViconDataStreamSDK();
viconClient = Client();

% Connects to a server
fprintf( 'Connecting to %s ...', opt.viconSettings.hostName );
while ~viconClient.IsConnected().Connected
    viconClient.Connect( opt.viconSettings.hostName );
    fprintf( '.' );
end
fprintf( '\n' );

% Enables some different data types
viconClient.EnableMarkerData();
fprintf( 'Marker Data Enabled: %s\n',              AdaptBool( viconClient.IsMarkerDataEnabled().Enabled ) );

% Sets the streaming mode
switch opt.viconSettings.StreamMode
    case 'ServerPush'
        viconClient.SetStreamMode( StreamMode.ServerPush );
    case 'ClientPull'
        viconClient.SetStreamMode( StreamMode.ClientPull );
    case 'ClientPullPreFetch'
        viconClient.SetStreamMode( StreamMode.ClientPullPreFetch );
    otherwise
        viconClient.SetStreamMode( StreamMode.ServerPush );
end
%% version_one
% testing how to plot fast
function az_function_v1(do_that,data,actFrame)

% first, check what to do
switch do_that
    case 'init'
        disp('....initializing objects within figure...');
        
    case 'draw'
        % tic
        cla;
        
        % some options
        nFrames = 40;
        props = {'EdgeColor','flat', 'FaceColor','none','LineWidth',5};
        
        % loop through all markers
        subjects = fieldnames(data);        
        if actFrame > 1
            for s = 1:numel(subjects)
                markers = fieldnames(data.(subjects{s}));
                for m = 1:numel(markers)
                    % adjust for less frames than _nFrames_ at the beginning
                    if actFrame < nFrames
                        plotFrames = actFrame;
                    else
                        plotFrames = nFrames;
                    end
                    
                    % set colormap according to plotted frames
                    colormap(copper(plotFrames));
                    
                    % fetch data and plot surface
                    d = data.(subjects{s}).(markers{m})(end-plotFrames+1:end,:);
                    surface([d(:,1),d(:,1)], [d(:,2),d(:,2)], [d(:,3),d(:,3)], ...
                        [(1:plotFrames)',(1:plotFrames)'], props{:});
%                         [(1:plotFrames)',(1:plotFrames)'], 'EdgeColor','flat', 'FaceColor','none','LineWidth',5);
                end
            end
        end
        % toc
        
    otherwise
        disp(' !------> wrong input, i do not know what to do right now... :(')
end
end
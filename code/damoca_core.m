%% damoca_core
% just the core:
%   - connect -> get data -> save data in 3D-array
%   - need subjects + segments + labeled markers
% 
% data_structure:  data.subject.marker = time * xyz   ///   dim: [t x 3]

clear all; close all;

%% options
% arena
opt.arena.sizeX = [0 5]; % set limits of x-axis of arena in [m] / will be calculated for simulated data
opt.arena.sizeY = [0 5]; % set limits of y-axis of arena in [m] / will be calculated for simulated data
opt.arena.sizeZ = [0 2]; % set limits of y-axis of arena in [m] / will be calculated for simulated data
opt.arena.viewAzimuth = 45;  % azimuth (left/right) angle of view (default: -37.5) [values = -180:180]
opt.arena.viewElevation = 13; % elevation (up/down) angle of view (default: 30) [values = 0:90]
opt.arena.fig_position = [95 1 1506 823];
% opt.arena.fig_position = [95 1 750 410];

% colors
opt.color.floor = [.2 .2 .2];
opt.color.wallA = [.5 .5 .5];
opt.color.wallB = [.55 .55 .55];
opt.color.space = [.3 .3 .3];

% stuff
opt.stuff.show_live_every_n_frame = 1;    % "3" means: display every third frame
opt.stuff.key_exit = 'ESC';

% vicon
opt.viconSettings.pathVicon_SDK = 'C:\Program Files\Vicon\DataStream SDK\Win64\MATLAB';
opt.viconSettings.hostName = '192.168.1.2:801';
opt.viconSettings.StreamMode = 'ClientPull'; % 'ServerPush' / 'ClientPull' / 'ClientPullPreFetch'

% video * JUST AVAILABLE WHEN USING SIMULATED DATA
opt.video.save = 1;
opt.video.framerate_input = 120;
opt.video.framerate_video = 60;
opt.video.video_quality = 80;
opt.video.filename = ['./ami_q' num2str(opt.video.video_quality) '_big.avi'];

% debug
debug.print = 0;

% do the real_deal OR simulate using some sample data
opt.simulate.do = 1;
opt.simulate.data = 'sample_ami.mat';
opt.simulate.margin = 0.1;      % add this margin (in m) to actual data in order to generate limits

%% checking input/options OR fixiing, if poosible
if opt.arena.viewElevation < 0 || opt.arena.viewElevation > 90
    disp('*** Elevation_Angle out of range *** exiting...');
    return
end

if opt.arena.viewAzimuth < -180 || opt.arena.viewAzimuth > 180
    disp('*** Azimuth_Angle out of range *** exiting...');
    return
end

%% init
frame = 0;
check_key = zeros(1,256);

if opt.simulate.do == 0
	fprintf('streaming data from vicon .... \n');

    addpath(opt.viconSettings.pathVicon_SDK);
    viconClient = vicon_connect(opt);
else
	fprintf('loading sample_data from file: 00 %%');
    sim = load(opt.simulate.data);
    sim_limits = [inf -inf;inf -inf;inf -inf];    
    % get subjects in sample_data
    sim.subjects = fieldnames(sim.data);
    for s = 1:numel(sim.subjects)
        % get markers for this subject
        sim.(['marker_s' num2str(s)]) = fieldnames(sim.data.(sim.subjects{s}));
        % search for min/max values of 3d data -> axis_limits
        for m = 1:numel(sim.(['marker_s' num2str(s)]))
            tmp_d = sim.data.(sim.subjects{s}).(sim.(['marker_s' num2str(s)]){m});
            for xyz = 1:3
                if min(tmp_d(:,xyz)) < sim_limits(xyz,1)
                    sim_limits(xyz,1) = min(tmp_d(:,xyz));
                end
                if max(tmp_d(:,xyz)) > sim_limits(xyz,2)
                    sim_limits(xyz,2) = max(tmp_d(:,xyz));
                end                
            end
        end
    end
    
    % update size of arena
    opt.arena.sizeX = [sim_limits(1,1) sim_limits(1,2)]./1000 + [-opt.simulate.margin opt.simulate.margin];
    opt.arena.sizeY = [sim_limits(2,1) sim_limits(2,2)]./1000 + [-opt.simulate.margin opt.simulate.margin];
    opt.arena.sizeZ = [0 sim_limits(3,2)]./1000 + [0 opt.simulate.margin];
    
    % get total number of frames
    nFrames = size(sim.data.(sim.subjects{1}).(sim.(['marker_s' num2str(1)]){1}),1);
    
    % init video
    if opt.video.save == 1
        video = VideoWriter(opt.video.filename);
        video.FrameRate = opt.video.framerate_video;
        video.Quality = opt.video.video_quality;
        open(video);
    
        vid_frames_out = round(linspace(1,nFrames,round(nFrames / opt.video.framerate_input * opt.video.framerate_video)));
    else
        vid_frames_out = 1:nFrames;
    end

end


%% generate figure
fgh = figure('Position',opt.arena.fig_position,'Color',opt.color.space,'Visible','on');
axh = axes('Parent',fgh); % create axes
axis equal; axis off;
xlim(opt.arena.sizeX .* 1000);
ylim(opt.arena.sizeY .* 1000);
zlim(opt.arena.sizeZ .* 1000);
view(opt.arena.viewAzimuth,opt.arena.viewElevation);
hold on;

draw_room(opt);

%% -------- insert supa dupa plottin stuff here ----------------------------------------------------
% pre_generate lines/subjects in plots to save computing time when drawing
% create handles for each element and use "set()" functions to edit data of object
% 
% e.g.: draw nan_line and give this line_handle to plot_functions 
%       lnh = plot(axh,NaN,NaN);

% -------- insert supa dupa plottin stuff here -----------------------------------------------------

%% loop - until "ESCAPE" is hit
% main loop through frames fetched from vicon, or frames in simulated data
while check_key(KbName(opt.stuff.key_exit)) ~= 1
    [~, ~, check_key] = KbCheck;
    frame = frame + 1;
    
    %% fetch data from vicon
    if opt.simulate.do == 0
        % Get a frame
        while viconClient.GetFrame().Result.Value ~= Result.Success; end
        
        % Get the latency
        if debug.print == 1, fprintf( 'Latency: %gs\n', viconClient.GetLatencyTotal().Total ); end
        
        % Count the number of subjects
        SubjectCount = viconClient.GetSubjectCount().SubjectCount;
        if debug.print == 1, fprintf( 'Subjects (%d):\n', SubjectCount ); end
        
        for SubjectIndex = 1:SubjectCount
            % Get the subject name
            SubjectName = viconClient.GetSubjectName( SubjectIndex ).SubjectName;
            if debug.print == 1, fprintf( '    Name: %s\n', SubjectName ); end
            
            % Count the number of markers
            MarkerCount = viconClient.GetMarkerCount( SubjectName ).MarkerCount;
            if debug.print == 1, fprintf( '    Markers (%d):\n', MarkerCount ); end
            
            for MarkerIndex = 1:MarkerCount
                % Get the marker name
                MarkerName = viconClient.GetMarkerName( SubjectName, MarkerIndex ).MarkerName;
                
                % Get the global marker translation
                Output_GetMarkerGlobalTranslation = viconClient.GetMarkerGlobalTranslation( SubjectName, MarkerName );
                
                % save xyz-position in data_table
                if Output_GetMarkerGlobalTranslation.Occluded == 1
                    data.(['subj' num2str(SubjectIndex)]).(MarkerName)(frame,:) = nan(1,3);
                else
                    data.(['subj' num2str(SubjectIndex)]).(MarkerName)(frame,:) = Output_GetMarkerGlobalTranslation.Translation(1:3);
                end
                
                % debug info
                if debug.print == 1
                    fprintf( '      Marker #%d: %s (%g, %g, %g) %s\n',                     ...
                        MarkerIndex - 1,                                    ...
                        MarkerName,                                         ...
                        Output_GetMarkerGlobalTranslation.Translation( 1 ), ...
                        Output_GetMarkerGlobalTranslation.Translation( 2 ), ...
                        Output_GetMarkerGlobalTranslation.Translation( 3 ), ...
                        AdaptBool( Output_GetMarkerGlobalTranslation.Occluded ) );
                end
            end % MarkerIndex
        end % subject loop
    else
        %% fetch simulated data
        fprintf('\b\b\b\b%02d %%', floor((frame / size(sim.data.(sim.subjects{1}).(sim.(['marker_s' num2str(1)]){1}),1))*100));
        for SubjectIndex = 1:numel(sim.subjects)
            for MarkerIndex = 1:numel(sim.(['marker_s' num2str(SubjectIndex)]) )
                this_subj = sim.subjects{SubjectIndex};
                this_marker = sim.(['marker_s' num2str(SubjectIndex)]){MarkerIndex};
                data.(this_subj).(this_marker)(frame,:) = sim.data.(this_subj).(this_marker)(frame,:);
            end
        end
        
        % check if file is ended
        if frame == nFrames
            check_key(KbName(opt.stuff.key_exit)) = 1;
        end
    end
    
    %% call plotting function
    % check if frame have to be plotted
    if opt.simulate.do == 0 && mod(frame,opt.stuff.show_live_every_n_frame)
        show = true;
    elseif opt.simulate.do == 1 && any(vid_frames_out == frame)
        show = true;
    else
        show = false;
    end
        
    % update figure
    if show
        % -------- insert supa dupa plottin stuff here ---------------------------------------------
        az_function_v1('draw',data,frame)
        % -------- insert supa dupa plottin stuff here ---------------------------------------------
        draw_room(opt);
        
        if opt.video.save == 1
            frameGET = getframe(fgh);
            writeVideo(video,frameGET);
        end
    end
end
if opt.simulate.do == 1, fprintf('\n'); end

%% video proessing
if opt.video.save == 1
    disp('saving video ... ');
	close(video);
end


%% exit VICON
if opt.simulate.do == 0
    vicon_exit(viconClient);
end

disp(' - - - - - - - - - - - - - -');
disp(' - - - F I N I S H E D - - -');
disp(' - - - - - - - - - - - - - -');

%% ---------------- sub_function ----------------------------------------
function draw_room(opt)
%% draw room
% floor
room.floor_x = [opt.arena.sizeX(1) opt.arena.sizeX(2) opt.arena.sizeX(2) opt.arena.sizeX(1)].*1000;
room.floor_y = [opt.arena.sizeY(1) opt.arena.sizeY(1) opt.arena.sizeY(2) opt.arena.sizeY(2)].*1000;
patch(room.floor_x,room.floor_y,opt.color.floor);

% wallA
if opt.arena.viewAzimuth > 0 && opt.arena.viewAzimuth < 180
    room.wallA_x = [opt.arena.sizeX(1) opt.arena.sizeX(1) opt.arena.sizeX(1) opt.arena.sizeX(1)].*1000; %%%
    room.wallA_y = [opt.arena.sizeY(1) opt.arena.sizeY(2) opt.arena.sizeY(2) opt.arena.sizeY(1)].*1000;
    room.wallA_z = [opt.arena.sizeZ(1) opt.arena.sizeZ(1) opt.arena.sizeZ(2) opt.arena.sizeZ(2)].*1000;
    patch(room.wallA_x,room.wallA_y,room.wallA_z,opt.color.wallA);
elseif opt.arena.viewAzimuth < 0 && opt.arena.viewAzimuth > -180
    room.wallA_x = [opt.arena.sizeX(2) opt.arena.sizeX(2) opt.arena.sizeX(2) opt.arena.sizeX(2)].*1000; %%%
    room.wallA_y = [opt.arena.sizeY(1) opt.arena.sizeY(2) opt.arena.sizeY(2) opt.arena.sizeY(1)].*1000;
    room.wallA_z = [opt.arena.sizeZ(1) opt.arena.sizeZ(1) opt.arena.sizeZ(2) opt.arena.sizeZ(2)].*1000;
    patch(room.wallA_x,room.wallA_y,room.wallA_z,opt.color.wallA);
end

% wallB
if opt.arena.viewAzimuth > -90 && opt.arena.viewAzimuth < 90
    room.wallB_x = [opt.arena.sizeX(1) opt.arena.sizeX(2) opt.arena.sizeX(2) opt.arena.sizeX(1)].*1000;
    room.wallB_y = [opt.arena.sizeY(2) opt.arena.sizeY(2) opt.arena.sizeY(2) opt.arena.sizeY(2)].*1000; %%%
    room.wallB_z = [opt.arena.sizeZ(1) opt.arena.sizeZ(1) opt.arena.sizeZ(2) opt.arena.sizeZ(2)].*1000;
    patch(room.wallB_x,room.wallB_y,room.wallB_z,opt.color.wallB);
elseif opt.arena.viewAzimuth > 90 || opt.arena.viewAzimuth < -90
    room.wallB_x = [opt.arena.sizeX(1) opt.arena.sizeX(2) opt.arena.sizeX(2) opt.arena.sizeX(1)].*1000;
    room.wallB_y = [opt.arena.sizeY(1) opt.arena.sizeY(1) opt.arena.sizeY(1) opt.arena.sizeY(1)].*1000; %%%
    room.wallB_z = [opt.arena.sizeZ(1) opt.arena.sizeZ(1) opt.arena.sizeZ(2) opt.arena.sizeZ(2)].*1000;
    patch(room.wallB_x,room.wallB_y,room.wallB_z,opt.color.wallB);
end
drawnow;
end
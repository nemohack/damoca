close all
load('sample_data.mat');
fields = fieldnames(data);
lag = 99;
incoming_data = 1;
start = 260;
T = 800;
figure()
plot3(1000, 1000, 1000)
hold on
plot3(-1000, -1000, -1000)
lim = 2500;
%xlim([-lim, lim]); ylim([-lim, lim]); zlim([-lim,lim]);
xlim([1150, 1800]); ylim([-2500, 2500]); zlim([0,800]);
while incoming_data
    for subjectIdx = 1 : numel(fields)
        subject = data.(fields{subjectIdx});
        m_fields = fieldnames(subject);
        for marker = 1 : numel(m_fields)
            current_data = subject.(m_fields{marker})(start - lag : start, :);
            plot3(current_data(:,1), current_data(:, 2), current_data(:,3), 'bo', 'MarkerSize', 18)
        end 
    end
    pause(0.005)
    cla
    start = start + 1;
%     drawnow
    %pause(0.005)
    if start == T
        incoming_data = 0;
    end
end